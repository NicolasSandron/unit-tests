/* eslint no-console: "off", no-restricted-imports: "off" */
import 'rxjs';

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';
import { useRef, useState, useMemo, useContext } from '../../src/__mocks__/react-hooks';
import React from '../../__mocks__/react';

process.env.DEBUG_MODE = false;

// Overwrite console.info to disable the following print message
// "Download the React DevTools for a better development experience"
console.info = () => {};

const mockReactWithHooks = ({
  autoUseEffectCleanUp = true,
  effect = false,
  state = false,
  ref = false,
  memo = false,
  context = false,
}) => {
  const buildUseEffectMock = autoCleanUp => {
    return jest.fn(callback => {
      const cleanUp = callback();

      if (typeof cleanUp === 'function') {
        if (autoCleanUp) {
          cleanUp();
        } else {
          global.useEffectCleanUp = callback();
        }
      }
    });
  };

  return {
    ...React,
    ...(effect && { useEffect: buildUseEffectMock(autoUseEffectCleanUp) }),
    ...(state && { useState }),
    ...(ref && { useRef }),
    ...(memo && { useMemo }),
    ...(context && { useContext }),
  };
};

global.mockReactWithHooks = mockReactWithHooks;

jasmine.getEnv().beforeEach(() => {
  console.error = (...args) => {
    throw new Error(...args);
  };

  global.__DEBUG_MODE__ = false;
  global.__LOGS__ = false;
  global.__MOCK__ = false;
  global.__CLIENT__ = true;
  global.__SERVER__ = false;
});

Object.defineProperty(window, 'localStorage', {
  writable: true,
});

Object.defineProperties(window.navigator, {
  userAgent: {
    value: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359',
    writable: true,
  },
});

Enzyme.configure({ adapter: new Adapter() });

jest.mock('../../dist/rev-manifest.json', () => ({}), { virtual: true });
jest.mock('../../dist/build-manifest.json', () => ({}), { virtual: true });
