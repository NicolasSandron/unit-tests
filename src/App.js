import React, { useState } from 'react';
import { validateCoupon } from './services/API';
import logo from './logo.svg';
import './App.css';

const doAlert = () => alert("Fuego");


function App() {
  const [value, setValue] = useState('');

  const isValidated = () => {
    return validateCoupon(value);
  }

  const hasError = () => {
    return value !== '' && !validateCoupon(value);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <button onClick={doAlert}>Fuego!</button>

      {isValidated() && <p className="validated">Validated</p>}
      {hasError() && <p className="error">Error</p>}

      <input type="text" onChange={(e) => setValue(e.target.value)} />

    </div>
  );
}

export default App;
