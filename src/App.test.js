import React from 'react';
import { validateCoupon } from './services/API';
import { shallow } from '../jest/enzyme-setup';
import App from './App';

jest.mock('./services/API', () => ({
  validateCoupon: jest.fn()
}));

let wrapper;

window.alert = jest.fn();

const defaultProps = {};

const getWrapper = () => {
wrapper = shallow(<App { ...defaultProps} />)};

describe('App', () => {
  beforeAll(() => {
    getWrapper();
  });

  it('contains a link', () => {
    expect(wrapper.find('a').length).toBe(1);
  });

  it('contains an image', () => {
    expect(wrapper.find('img').length).toBe(1);
  });

  it('does not contain a title', () => {
    expect(wrapper.find('h1').length).toBe(0);
  });

  it('contains a button', () => {
    expect(wrapper.find('button').length).toBe(1);
  });

  describe('when clicking a button', () => {
    it('shows an alert', () => {
      wrapper.find('button').simulate('click');
      expect(window.alert).toHaveBeenCalled();
    });
  });

  describe('when entering a coupon', () => {
    describe('and the the user has not entered the value yet', () => {
      it('does not show a validation message', () => {
        expect(wrapper.find('.validated').length).toBe(0);
      });

      it('does not show an error message', () => {
        expect(wrapper.find('.error').length).toBe(0);
      });
    });

    describe('and the coupon is validated', () => {
      beforeEach(() => {
        // Return value
        validateCoupon.mockReturnValue(true);
      })

      it('shows a validation message', () => {
        // Trigger change
        expect(wrapper.find('input').simulate('change', { target: { value: 'tata' } }));
        expect(wrapper.find('.validated').length).toBe(1);
        expect(wrapper.find('.error').length).toBe(0);
      })
    });

    describe('and the coupon is not validated', () => {
      beforeEach(() => {
        // Return value
        validateCoupon.mockReturnValue(false);
      });

      it('shows an error message', () => {
        // Trigger change
        expect(wrapper.find('input').simulate('change', { target: { value: 'tata' } }));
        expect(wrapper.find('.validated').length).toBe(0);
        expect(wrapper.find('.error').length).toBe(1);
      });
    })
  });
});
